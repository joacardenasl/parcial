<?php

$lenguaje = "";
if(isset($_POST["lenguaje"])){
    $lenguaje = $_POST["lenguaje"];
}
$nombre = "";
if(isset($_POST["nombre"])){
    $nombre = $_POST["nombre"];
}
$fecha = "";
if(isset($_POST["fecha"])){
    $fecha = $_POST["fecha"];
}    
if(isset($_POST["crear"])){
    $app = new APP("", $lenguaje, $nombre, $fecha);
    $app -> insertar();    
}
?>
<head>
<head>
<link rel="icon" type="image/png" href="img/logo.png" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
	
</head>
</head>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Insertar App</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="presentacion/app/insertarApp.php" method="post">
						<div class="form-group">
							<label>Lenguaje</label> 
							<input type="text" name="lenguaje" class="form-control"required>
						</div>
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Fecha</label> 
							<input type="text" name="fecha" class="form-control"required>
						</div>
						<button type="submit" name="crear" class="btn btn-info">Crear</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>