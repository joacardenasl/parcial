<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/AppDAO.php";
class App{
    private $idapp;
    private $lenguaje;
    private $nombre;
    private $fecha;
    private $conexion;
    private $appDAO;

    public function getIdapp(){
        return $this -> idapp;
    }
    public function getLenguaje(){
        return $this -> lenguaje;
    }

    public function getNombre(){
        return $this -> nombre;
    }
    public function getFecha(){
        return $this -> fecha;
    }


    public function App($idapp = "", $lenguaje = "", $nombre = "", $fecha = ""){
        $this -> idapp = $idapp;
        $this -> lenguaje = $lenguaje;
        $this -> nombre = $nombre;
        $this -> fecha = $fecha;
        $this -> conexion = new Conexion();
        $this -> appDAO = new AppDAO($this -> idapp, $this -> lenguaje, $this -> nombre, $this -> fecha);
    }
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> appDAO -> insertar());        
        $this -> conexion -> cerrar();        
    }

    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> appDAO -> consultarPaginacion($cantidad, $pagina));
        $apps = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new App($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($apps, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
}

?>